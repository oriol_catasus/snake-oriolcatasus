package cat.escolapia.damviod.pmdm.snake;

/**
 * Created by oriol.catasus on 07/12/2016.
 */
public class WallPart {
    public int x,y;

    public WallPart(int _x, int _y) {
        x = _x;
        y = _y;
    }
}
