package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;
import cat.escolapia.damviod.pmdm.framework.Music;

public class Assets {
    public static Pixmap[] backgroundLvl = new Pixmap[10];
    public static Pixmap background;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap Name;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap headUp;
    public static Pixmap headLeft;
    public static Pixmap headDown;
    public static Pixmap headRight;
    public static Pixmap tail;
    public static Pixmap wall;
    public static Pixmap diamond;

    public static Sound click;
    public static Sound eat;
    public static Sound xoc;
    public static Music music;
}
