package cat.escolapia.damviod.pmdm.snake;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;

/**
 * Created by oriol.catasus on 30/11/2016.
 */
public class Credits extends Screen {
    public Credits(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        //This is for check if the player click in the go back button
        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if (event.type == Input.TouchEvent.TOUCH_UP) {
                if (event.x < 64 && event.y > 416) {
                    Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    };

    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.background, 0, 0);
        g.drawPixmap(Assets.Name, 64, 64);  //Here is when I render the Credits image
        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64);
    }

    public void pause() {
        Assets.music.pause();
    }

    public void resume() {
        Assets.music.play();
    }

    public void dispose() {

    }
}
