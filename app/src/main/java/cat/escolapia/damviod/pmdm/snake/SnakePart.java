package cat.escolapia.damviod.pmdm.snake;

public class SnakePart {
    public int x, y;
    
    public SnakePart(int x, int y) {
        this.x = x;
        this.y = y;
    }
}       
