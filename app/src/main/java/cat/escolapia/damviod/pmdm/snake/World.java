package cat.escolapia.damviod.pmdm.snake;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 15;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.05f;
    public Snake snake;
    public Diamond[] diamondArray = new Diamond[2];
    public List<Wall> wallList = new ArrayList<Wall>();
    int maxLenghtWall = 3;
    public boolean gameOver = false;
    public int score = 0, scoreInitLvl = 0, scoreForNextLvl = 30;
    public int lvl = 1;
    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        snake = new Snake();
        wallList.add(new Wall());
        placeWall(0, 1);
        placeDiamond(0);
        placeDiamond(1);
    }

    void placeWall(int j, int size) {   // This method uses as place a Wall in a random free space of the screen.
        initBoolArray();                // The first parameter is the position of the Wall in the wallList
        int wallX, wallY;               // The second parameter is the length of the wall
        boolean vertical;
        while (true) {
            vertical = random.nextBoolean();
            wallX = random.nextInt(WORLD_WIDTH);
            wallY = random.nextInt(WORLD_HEIGHT);
            if (vertical && wallY > WORLD_HEIGHT - size) {
                wallY = WORLD_HEIGHT - size;
            } else if (!vertical && wallX > WORLD_WIDTH - size) {
                wallX = WORLD_WIDTH - size;
            }
            if (!fields[wallX][wallY]) {
                break;  //Break the loop  when coordinates are false
            }
        }
        wallList.get(j).wallPartList.clear();
        for (int i = 0; i < size; i++) {
            if (vertical) {
                wallList.get(j).wallPartList.add(new WallPart(wallX, wallY + i));
            } else {
                wallList.get(j).wallPartList.add(new WallPart(wallX + i, wallY));
            }
        }
    }

    void initBoolArray() {
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }
        int len = snake.parts.size();
        for (int i = 0; i < len; i++) { //Change snake coordinates to true
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }
        for (int i = 0; i < diamondArray.length; i++) { //Change diamonds coordinates to true
            if (diamondArray[i] != null) {
                fields[diamondArray[i].x][diamondArray[i].y] = true;
            }
        }
        for (int i = 0; i < wallList.size(); i++) { //Change wall coordinates to true
            for (int j = 0; j < wallList.get(i).wallPartList.size(); j++) {
                fields[wallList.get(i).wallPartList.get(j).x][wallList.get(i).wallPartList.get(j).y] = true;
            }
        }
    }

    private void placeDiamond(int j) { //The number passed by parameter is the position of the diamond in the diamondArray
        initBoolArray();
        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;
            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
        }
        diamondArray[j] = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
    }

    public void update(float deltaTime) {
        if (gameOver) {
            Settings.addScore(score);
            return;
        }
        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            checkDiamond();
            checkWall();
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            //Comprovació x i y dels head igual a diamant.
            // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
            // es pot baixar el tick per augmentar la dificultat
        }
    }

    void checkDiamond() {   //This method check if the head of the snake is in the same place as one of the diamonds
        for (int i = 0; i < diamondArray.length; i++) {
            if (diamondArray[i].x == snake.getSnakePart(0).x && diamondArray[i].y == snake.getSnakePart(0).y) {
                Assets.eat.play(1);
                placeDiamond(i);
                ScoreIncrement();
                return;
            }
        }
    }

    //Check if the head of the snake is in the same place as one of the walls
    void checkWall() {
        for (int j = 0; j < wallList.size(); j++) {
            for (int i = 0; i < wallList.get(j).wallPartList.size(); i++) {
                if (wallList.get(j).wallPartList.get(i).x == snake.getSnakePart(0).x && wallList.get(j).wallPartList.get(i).y == snake.getSnakePart(0).y) {
                    Assets.xoc.play(1);
                    gameOver = true;
                    return;
                }
            }
        }

    }

    void SpeedIncrement() { //This method increase the speed of the snake with reducing time between updates.
        if (tick > TICK_DECREMENT) {
            tick -= TICK_DECREMENT;
            if (tick < TICK_DECREMENT) { //With this if I can be sure that tick never will be smaller than TICK_DECREMENT
                tick = TICK_DECREMENT;
            }
        }
    }

    //This method increment the score and, if the score is enought, increment the lvl too.
    void ScoreIncrement() {
        score += SCORE_INCREMENT;
        if (score - scoreInitLvl >= scoreForNextLvl) {  //When lvl up, speed, snake length and walls length or number is increased, and the walls're placed in another pos
            scoreInitLvl = score;
            lvl++;
            SpeedIncrement();
            snake.allarga();
            for (int i = 0; i < wallList.size(); i++) {
                if (i == wallList.size() - 1) {     //wallList.size() - 1 is the last wall added to the list
                    if (wallList.get(i).wallPartList.size() < maxLenghtWall) {
                        placeWall(i, wallList.get(i).wallPartList.size() + 1);
                    } else {    //If the last wall added lenght is equal than maxLenghtWall, then add another wall with only 1 part
                        wallList.add(new Wall());
                        placeWall(i + 1, 1);
                        return;
                    }
                } else {    //If the wall isn't the last added, then only change his position
                    placeWall(i, wallList.get(i).wallPartList.size());
                }
            }
        }
    }
}
