package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {

        Graphics g = game.getGraphics();

        Assets.backgroundLvl[0] = g.newPixmap("Backgrounds/Background1.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[1] = g.newPixmap("Backgrounds/Background2.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[2] = g.newPixmap("Backgrounds/Background3.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[3] = g.newPixmap("Backgrounds/Background4.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[4] = g.newPixmap("Backgrounds/Background5.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[5] = g.newPixmap("Backgrounds/Background6.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[6] = g.newPixmap("Backgrounds/Background7.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[7] = g.newPixmap("Backgrounds/Background8.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[8] = g.newPixmap("Backgrounds/Background9.jpg", PixmapFormat.RGB565);
        Assets.backgroundLvl[9] = g.newPixmap("Backgrounds/Background10.jpg", PixmapFormat.RGB565);

        Assets.background = g.newPixmap("background.png", PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("logo.png", PixmapFormat.ARGB4444);
        Assets.mainMenu = g.newPixmap("mainmenu.png", PixmapFormat.ARGB4444);
        Assets.Name = g.newPixmap("Name.png", PixmapFormat.RGB565);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("numbers.png", PixmapFormat.ARGB4444);
        Assets.ready = g.newPixmap("ready.png", PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pausemenu.png", PixmapFormat.ARGB4444);
        Assets.gameOver = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.headUp = g.newPixmap("headup.png", PixmapFormat.ARGB4444);
        Assets.headLeft = g.newPixmap("headleft.png", PixmapFormat.ARGB4444);
        Assets.headDown = g.newPixmap("headdown.png", PixmapFormat.ARGB4444);
        Assets.headRight = g.newPixmap("headright.png", PixmapFormat.ARGB4444);
        Assets.tail = g.newPixmap("tail.png", PixmapFormat.ARGB4444);
        Assets.wall = g.newPixmap("wall.png", PixmapFormat.ARGB4444);
        Assets.diamond = g.newPixmap("diamond.png", PixmapFormat.ARGB4444);
        Assets.click = game.getAudio().newSound("click.ogg");
        Assets.eat = game.getAudio().newSound("eat.ogg");
        Assets.xoc = game.getAudio().newSound("bitten.ogg");
        Assets.music = game.getAudio().newMusic("Music/Snake III - Main Theme.mp3");

        Settings.load(game.getFileIO());

        Assets.music.setLooping(true);  //Put the music in loop mode and play it
        Assets.music.play();

        game.setScreen(new MainMenuScreen(game));
    }
    
    public void render(float deltaTime) {

    }

    public void pause() {
    }

    public void resume() {
    }

    public void dispose() {

    }
}
